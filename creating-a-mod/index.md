>[!CAUTION]
> # Todo for this page
> 1. Improve starting paragraph
> 2. Explain the overall process of modding (what the mod loader does and where the modders code stands)
> 3. Explain if they have any other questions where they can find the answers (unity docs, modding discord etc)

This will cover the basics of how to create your own mod from installing the software needed, to building and injecting your first mod. 