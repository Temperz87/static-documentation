# Software

- Visual Studio with .net desktop development workload
- Unity 2019.1.8 (Optional)

## Visual Studio
You can download Visual Studio for free from the Microsoft website , when installing make sure to include the ".NET desktop development".

![](/images/creating-a-mod/netdesktopdevelopmentworkload.PNG)

This should give you access to create a Class Library in the .NET framework later on. 
![](/images/creating-a-mod/ClassLibrary.PNG)

## Unity 2018.1.8 (Optional)

You don't need Unity, however, if you want to start importing your own assets into the game then you will need to create asset bundles inside of Unity. I have recommended 2019.1.8 because it is the closest version to the current version of VTOL VR is made in. You can download that version of Unity from [their website here](https://unity3d.com/get-unity/download/archive). Go to Unity 2019.X at the top, then scroll to Unity 2019.1.8 and download the installer.
![](/images/creating-a-mod/unity.PNG)

Once these are installed, we can next move onto creating a basic mod in Visual Studio.
