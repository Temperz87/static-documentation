Use this to replace a method. The return false stops the original method.
```cs
[HarmonyPatch(typeof(ClassName))]
[HarmonyPatch("MethodName")]
public class Patch0
{
    public static bool Prefix()
    {
        //Your Custom Code
        return false;
    }
}
```