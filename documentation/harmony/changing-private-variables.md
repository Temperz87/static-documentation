Use this snippet to get and set private variables.
```cs
var foo = FindObjectOfType<CustomClass>();
Traverse.Create(foo).Field("privateVariableName").SetValue("world");
```