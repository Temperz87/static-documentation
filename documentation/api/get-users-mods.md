If you need to check what mods the user has loaded, you can call GetUsersMods() to get a list of the type Mod which has the basic information about that mod (and some other stuff the mod loader uses) It also displays them in order which they were loaded. 

```cs
public override void Start()
{
    List<Mod> mods = VTOLAPI.GetUsersMods();
    for (int i = 0; i < mods.Count; i++)
    {
        Log($"{mods[i].name} dll is located at {mods[i].dllPath}");
    }
}
```